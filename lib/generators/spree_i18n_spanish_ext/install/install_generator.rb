module SpreeI18nSpanishExt
  module Generators
    class InstallGenerator < Rails::Generators::Base

      def add_javascripts
        append_file 'app/assets/javascripts/store/all.js', "//= require store/spree_i18n_spanish_ext\n"
        append_file 'app/assets/javascripts/admin/all.js', "//= require admin/spree_i18n_spanish_ext\n"
      end

      def add_stylesheets
        inject_into_file 'app/assets/stylesheets/store/all.css', " *= require store/spree_i18n_spanish_ext\n", :before => /\*\//, :verbose => true
        inject_into_file 'app/assets/stylesheets/admin/all.css', " *= require admin/spree_i18n_spanish_ext\n", :before => /\*\//, :verbose => true
      end

      def add_seed_data
        append_file "db/seeds.rb", "\nSpreeI18nSpanishExt::Engine.load_seed if defined?(SpreeI18nSpanishExt)\n"
      end

      def run_seeds
         res = ask 'Would you like to run seeds now? [Y/n]'
         if res == '' || res.downcase == 'y'
           run 'bundle exec rake db:seed'
         else
           puts 'Skipping rake db:seed, don\'t forget to run it!'
         end
      end

    end
  end
end
