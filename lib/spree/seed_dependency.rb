module Spree
  module SeedDependency
    def self.load_seed(file)
      path = File.expand_path(samples_path + "#{file}.rb")
      # Check to see if the specified file has been loaded before
      if !$LOADED_FEATURES.include?(path)
        require path
      end
    end

    private
      def self.samples_path
        Pathname.new(File.join(File.dirname(__FILE__), '..', '..', 'db', 'seeds', 'spree'))
      end
  end
end