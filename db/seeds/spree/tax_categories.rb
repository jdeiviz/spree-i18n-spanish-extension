#encoding: utf-8
Spree::TaxCategory.create!(:name => "Iva General", :is_default => true)
Spree::TaxCategory.create!(:name => "Iva Reducido")
Spree::TaxCategory.create!(:name => "Iva Superreducido")
