#encoding: utf-8

location = Spree::StockLocation.first_or_create! name: 'Por defecto'
location.active = true
location.country =  Spree::Country.where(iso: 'ES').first
location.save!