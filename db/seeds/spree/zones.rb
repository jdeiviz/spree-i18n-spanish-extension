#encoding: utf-8
eu_vat = Spree::Zone.create!(:name => "Europa", :description => "Zona Europa")
north_america = Spree::Zone.create!(:name => "América del Nortge", :description => "USA + Canada")
spain = Spree::Zone.create!(:name => "España", :description => "Zona España", :default_tax => true)

["Polonia", "Finlandia", "Portugal", "Rumania", "Alemania", "Francia",
 "Eslovaquia", "Hungría", "Eslovenia", "Irlanda", "Austria",
 "Italia", "Belgica", "Suecia", "Letonia", "Bulgaria", "Reino Unido",
 "Lituania", "Chipre", "Luxemburgo", "Malta", "Dinamarca", "Países Bajos",
 "Estonia"].
each do |name| 
  eu_vat.zone_members.create!(:zoneable => Spree::Country.find_by_name!(name))
end

["Estados Unidos", "Canadá"].each do |name|
  north_america.zone_members.create!(:zoneable => Spree::Country.find_by_name!(name))
end

["España"].each do |name|
  spain.zone_members.create!(:zoneable => Spree::Country.find_by_name!(name))
end