#encoding: utf-8
Spree::SeedDependency.load_seed("zones")

spain = Spree::Zone.find_by_name!("España")

iva_general = Spree::TaxCategory.find_by_name!("Iva General")
tax_rate = Spree::TaxRate.create({
    :name => "Iva General",
    :zone => spain, 
    :amount => 0.21,
    :tax_category => iva_general,
    :included_in_price => true,
    :show_rate_in_label => false,
  },
  :without_protection => true)
tax_rate.calculator = Spree::Calculator::DefaultTax.create!
tax_rate.save!

iva_reducido = Spree::TaxCategory.find_by_name!("Iva Reducido")
tax_rate = Spree::TaxRate.create({
    :name => "Iva Reducido",
    :zone => spain, 
    :amount => 0.10,
    :tax_category => iva_reducido,
    :included_in_price => true,
    :show_rate_in_label => false
  },
  :without_protection => true)
tax_rate.calculator = Spree::Calculator::DefaultTax.create!
tax_rate.save!

iva_superreducido = Spree::TaxCategory.find_by_name!("Iva Superreducido")
tax_rate = Spree::TaxRate.create({
    :name => "Iva Superreducido",
    :zone => spain, 
    :amount => 0.04,
    :tax_category => iva_superreducido,
    :included_in_price => true,
    :show_rate_in_label => false
  },
  :without_protection => true)
tax_rate.calculator = Spree::Calculator::DefaultTax.create!
tax_rate.save!