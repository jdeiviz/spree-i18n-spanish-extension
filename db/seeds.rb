Spree::Calculator.delete_all
Spree::Zone.delete_all
Spree::ZoneMember.delete_all
Spree::TaxRate.delete_all
Spree::TaxCategory.delete_all
Spree::State.delete_all
Spree::Country.delete_all
Spree::ShippingCategory.delete_all
Spree::StockLocation.delete_all

seeds_path = File.join(File.dirname(__FILE__), 'seeds')
Rake::Task['db:load_dir'].reenable
Rake::Task['db:load_dir'].invoke(seeds_path)