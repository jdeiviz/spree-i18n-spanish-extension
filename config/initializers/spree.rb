Spree.config do |config|  
  config.currency = 'EUR'
  config.currency_symbol_position = 'after'
  config.currency_decimal_mark = ','
  config.currency_thousands_separator = '.'  
  config.display_currency = false
  config.prices_inc_tax = true
  config.shipment_inc_vat = true
end
